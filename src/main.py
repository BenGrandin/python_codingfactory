from src import helpers


def encode_uri(string):
    """ Encode a string to base64
    Args:
        string: a string
    Return:
        A base64 string
    """
    # step 1
    string_as_a_list = helpers.convert_string_to_list(string)
    # print(string_as_a_list)

    # step 2
    int_list = helpers.convert_list_to_int_list(string_as_a_list)
    # print(int_list)

    # step 3
    an_8_bits_list = helpers.convert_int_list_to_8_bits_list(int_list)
    # print(an_8_bits_list)

    # step 4
    bit_string = helpers.convert_list_to_string(an_8_bits_list)
    # print(bit_string)

    # step 5
    list_with_6_bits = \
        helpers.convert_string_to_bit_list(bit_string, 6)
    # print(list_with_6_bits)

    # step 6
    list_with_6_int = helpers \
        .convert_bit_list_to_unicode_list(list_with_6_bits)
    # print(list_with_6_int)

    # step 7
    base64_list = helpers \
        .convert_int_list_to_base64_list(list_with_6_int)
    # print(base64_list)

    # step 8
    list_with_length_is_multiple_of_4 = helpers \
        .convert_base64_list_to_list_with_length_is_mutliple_of_4(
            base64_list)
    # print(list_with_length_is_multiple_of_4)

    # step 9
    encode_string = helpers \
        .convert_list_to_string(list_with_length_is_multiple_of_4)
    # print(encode_string)

    return encode_string


def decode_uri(string):
    """ Decode a string to base64
    Args:
        string: a base64 string
    Return:
        A utf-8 string
    """

    # Step 1 : remove all '='
    string_without_equal = helpers.replace_char_from_string(string, "=")
    # print(string_without_equal)

    # Step 2 : replace all base64 char by int
    int_list = helpers.convert_string_base64_to_list_int(
        string_without_equal)
    # print(int_list)

    # Step 3 : replace all base64 char by int
    list_6_bits = helpers.convert_list_int_to_list_6_bits(int_list)
    # print(list_6_bits)

    # Step 4 : String with length % 8 = 0
    string_multiple_of_8 = helpers \
        .convert_list_to_string_with_length_mod_number_is_0(list_6_bits, 8)
    # print(string_multiple_of_8)

    # Step 5 : Convert string to list with bits
    list_with_8_bits = helpers.convert_string_to_bit_list(
        string_multiple_of_8, 8)
    # print(list_with_8_bits)

    # Step 6 : Convert list with bit to list with unicode
    unicode_list = helpers \
        .convert_bit_list_to_unicode_list(list_with_8_bits)
    # print(unicode_list)

    # Step 7 : Convert list unicode to list with char
    list_with_char = helpers \
        .unicode_list_to_convert_list_with_char(unicode_list)
    # print(list_with_char)

    return helpers.convert_list_to_string(list_with_char)
